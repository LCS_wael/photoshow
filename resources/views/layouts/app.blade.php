<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PhotoShow</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.css" integrity="sha512-2wTbASMBE2bsgS3MDLj4bqdbFkagTT3iLNw2pV8hsT9VtZ1FO5DmDmgsmBgIhPB2GLP7RVFQbx3nBCxTAi1oxA==" crossorigin="anonymous" />
    </head>
    <body>
    @include('inc.topbar')
    <br>
    <div class="row">
        @include('inc.messages')
        @yield('content')
    </div>
    </body>
</html>
